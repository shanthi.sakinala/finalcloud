# S3 outputs

output "region" {
    value   = var.region
}

output "bucket_name" {
    value   = var.bucket_name
}

