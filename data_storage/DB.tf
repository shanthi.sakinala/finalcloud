# Database

    # RDS - MySQL
resource "aws_db_instance" "db" {
    identifier          = "wordpress"
    name                = "wordpress"
    allocated_storage   = 20
    storage_type        = "gp2"
    instance_class      = "db.t2.micro"
    engine              = "mysql"
    engine_version      = "5.7"

    username            = var.db_username
    password            = var.db_password


#   multi_az                    = true
    skip_final_snapshot         = true

    vpc_security_group_ids      = [data.terraform_remote_state.networking.outputs.security_group_db]
    db_subnet_group_name        = aws_db_subnet_group.dbgroup.id

    tags = {
        Name    = "DB"
    }
}

    # DB subnet group
resource "aws_db_subnet_group" "dbgroup" {
    name        = "dbgroup"
    subnet_ids  = data.terraform_remote_state.networking.outputs.db_subnets



}

