# Data import from networking

data "terraform_remote_state" "networking" {
    backend = "s3"
    config = {
        bucket  = "finalcloud-state-files"
        key     = "networking.tf"
        region  = "eu-west-3"
    }
}