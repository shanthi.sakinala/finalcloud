# Outputs

    # Base network and instances

output "vpc_id" {
    value   = aws_vpc.wordpress.id 
}

output "region" {
    value   = var.region
}

output "bucket_name" {
    value   = local.bucket_name
}

    # Ports

output "server_port" {
    value   = var.server_port
}

    # Subnets

output "dmz_subnets" {
    value   = local.dmz_subnets
}

output "apps_subnets" {
    value   = local.apps_subnets
}

output "db_subnets" {
    value   = local.db_subnets
}

    # Security groups

output "security_group_apps" {
    value = aws_security_group.apps.id
}

output "security_group_dmz" {
    value = aws_security_group.dmz.id
}

output "security_group_lb" {
    value = aws_security_group.lb.id
}

output "security_group_db" {
    value = aws_security_group.db.id
}

    # CIDR blocks

output "dmz_cidr" {
    value   = local.dmz_cidr
}



