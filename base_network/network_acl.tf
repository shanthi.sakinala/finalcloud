# Network ACLs

    # Private

resource "aws_network_acl" "private" {
    vpc_id  = aws_vpc.wordpress.id
    subnet_ids = concat(local.apps_subnets, local.db_subnets)

    ingress {
        protocol    = "-1"
        rule_no     = 100
        action      = "allow"
        cidr_block  = "0.0.0.0/0"
        from_port   = 0
        to_port     = 0
    }

    
    egress {
        protocol    = "-1"
        rule_no     = 200
        action      = "allow"
        cidr_block  = "0.0.0.0/0"
        from_port   = 0
        to_port     = 0
    }

    tags = {
        Name    = "private"
    }

}


    # Public

resource "aws_network_acl" "public" {
    vpc_id  = aws_vpc.wordpress.id
    subnet_ids = local.dmz_subnets

    ingress {
        protocol    = "-1"
        rule_no     = 100
        action      = "allow"
        cidr_block  = "0.0.0.0/0"
        from_port   = 0
        to_port     = 0
    }
        
    egress {
        protocol    = "-1"
        rule_no     = 200
        action      = "allow"
        cidr_block  = "0.0.0.0/0"
        from_port   = 0
        to_port     = 0
    }

    tags = {
        Name    = "public"
    }
}