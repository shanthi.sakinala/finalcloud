# Subnets

    # apps

resource "aws_subnet" "az1" {
    vpc_id                  = aws_vpc.wordpress.id 
    cidr_block              = "10.0.1.0/24"
    availability_zone       = "${var.region}a"
    map_public_ip_on_launch = false

    tags = {
        Name = "apps_az1"
    }
}

resource "aws_subnet" "az2" {
    vpc_id                  = aws_vpc.wordpress.id 
    cidr_block              = "10.0.2.0/24"
    availability_zone       = "${var.region}b"
    map_public_ip_on_launch = false

    tags = {
        Name = "apps_az2"
    }
}

resource "aws_subnet" "az3" {
    vpc_id                  = aws_vpc.wordpress.id 
    cidr_block              = "10.0.3.0/24"
    availability_zone       = "${var.region}c"
    map_public_ip_on_launch = false

    tags = {
        Name = "apps_az3"
    }
}

    # Back

resource "aws_subnet" "dbaz1" {
    vpc_id                  = aws_vpc.wordpress.id 
    cidr_block              = "10.0.4.0/24"
    availability_zone       = "${var.region}a"
    map_public_ip_on_launch = false

    tags = {
        Name = "back_az1"
    }
}

resource "aws_subnet" "dbaz2" {
    vpc_id                  = aws_vpc.wordpress.id 
    cidr_block              = "10.0.5.0/24"
    availability_zone       = "${var.region}b"
    map_public_ip_on_launch = false

    tags = {
        Name = "back_az2"
    }
}

resource "aws_subnet" "dbaz3" {
    vpc_id                  = aws_vpc.wordpress.id 
    cidr_block              = "10.0.6.0/24"
    availability_zone       = "${var.region}c"
    map_public_ip_on_launch = false

    tags = {
        Name = "back_az3"
    }
}


    #DMZ

resource "aws_subnet" "dmz1" {
    vpc_id                  = aws_vpc.wordpress.id
    cidr_block              = "10.0.7.0/24"
    availability_zone       = "${var.region}a"
    map_public_ip_on_launch = true

    tags = {
        Name = "dmz1"
    }
}

resource "aws_subnet" "dmz2" {
    vpc_id                  = aws_vpc.wordpress.id
    cidr_block              = "10.0.8.0/24"
    availability_zone       = "${var.region}b"
    map_public_ip_on_launch = true

    tags = {
        Name = "dmz2"
    }
}

resource "aws_subnet" "dmz3" {
    vpc_id                  = aws_vpc.wordpress.id
    cidr_block              = "10.0.9.0/24"
    availability_zone       = "${var.region}c"
    map_public_ip_on_launch = true

    tags = {
        Name = "dmz3"
    }
}
