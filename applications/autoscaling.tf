# Autoscaling groups


    # Front

resource "aws_autoscaling_group" "main" {
    name                        = "main_ASG"    
    launch_configuration        = aws_launch_configuration.main.id
    vpc_zone_identifier         = data.terraform_remote_state.networking.outputs.apps_subnets
    health_check_grace_period   = 60
    target_group_arns           = [aws_lb_target_group.target.id]
    health_check_type           = "ELB"
    
    min_size                    = 1
    max_size                    = 12

    lifecycle {
        create_before_destroy   = true
    }
}

    # Bastion
resource "aws_autoscaling_group" "bastion" {
    name                    = "bastion"
    launch_configuration    = aws_launch_configuration.dmz.id
    vpc_zone_identifier     = data.terraform_remote_state.networking.outputs.dmz_subnets
    target_group_arns       = [aws_lb_target_group.target.id]
    health_check_type       = "ELB"


    min_size            = 1
    max_size            = 6
    desired_capacity    = 1

    lifecycle {
        create_before_destroy = true
    }  
}