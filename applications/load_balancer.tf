
# Load balancer

resource "aws_lb" "lb" {
    name    = "lb"
    load_balancer_type  = "application"
    security_groups     = [data.terraform_remote_state.networking.outputs.security_group_lb]
    subnets             = data.terraform_remote_state.networking.outputs.dmz_subnets

    tags = {
        Name = "lb"
    }
}

    # Listener

resource "aws_lb_listener" "http" {
    load_balancer_arn   = aws_lb.lb.arn 
    port                = data.terraform_remote_state.networking.outputs.server_port
    protocol            = "HTTP"

    default_action {
      type = "redirect"

      redirect {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
    }
  }
}

    # Target group

resource "aws_lb_target_group" "target" {
    vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id
    name        = "target-group"
    port        = data.terraform_remote_state.networking.outputs.server_port
    protocol    = "HTTP"
    
    health_check {
        path                = "/"
        protocol            = "HTTP"
        matcher             = "200"
        interval            = 15
        timeout             = 3
        healthy_threshold   = 3
        unhealthy_threshold = 2
    }
}