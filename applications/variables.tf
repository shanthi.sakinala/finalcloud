variable "instance_type" {
    default = "t2.micro"
}

variable "bastion_type" {
    default = "t2.micro"
}

variable "image_id" {
     default = "ami-04552009264cbe9f4" #centos 7 official
}

variable "key_name" {
    default = "wordpress2"
}

variable "region" {
    default = "eu-west-3"
}